# css-tarefa


O objetivo da tarefa é aplicar todos os conceitos aprendidos nas aulas de CSS
na sua página individual da tarefa de HTML utilizando o link do FIGMA abaixo
como layout de referência (Essa tarefa é individual)
Link do figma com o layout:
FIGMA
Pré-requisitos:
● Ter terminado e entregue a tarefa de HTML
Requisitos da tarefa:
● Seguir e implementar o layout proposto
● EXTRA Ter pelo menos duas animações distintas usando o :hover
● EXTRA fazer um menu Hambúrguer (aquele que costumamos ver em
sites mobile com 3 linhas horizontais que expandem ao ser clicado)
funcional usando somente HTML e CSS
Regras:
1. Usar apenas HTML e CSS.
2. A página deve ser responsiva.
3. Você é livre para utilizar qualquer fonte, imagens e paleta de cores do
seu agrado